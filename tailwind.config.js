module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      keyframes: {
        fadein: {
          '0%': { bottom: 0, opacity: 0, },
          '100%': { opacity: 1 },
        },
        fadeout: {
          '0%': { opacity: 1 },
          '100%': { bottom: 0, opacity: 0 },
        }
      },
      animation: {
        popout: 'fadein 0.5s, fadeout 0.5s 2.5s'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
