import dayjs from 'dayjs';

import axios from '../axios';
import { ApiCategory } from './category';
import { ApiLocation } from './location';
import { ApiPaymentMethod } from './paymentMethod';

export interface ApiExpense {
  id?: number;
  amount: number;
  date: string;

  categoryId: number;
  category?: ApiCategory;

  locationId: number;
  location?: ApiLocation;

  paymentMethodId: number;
  paymentMethod?: ApiPaymentMethod;

  notes: string|null;
}


/**
 * Transforms an API expense object to a client Expense object.
 * apiExpense must have full category, location and paymentMethod
 * api objects, otherwise this method will throw an error.
 *
 * @param apiExpense A *full* API expense object
 */
function apiExpenseToExpense(apiExpense: ApiExpense): Expense {
  if (!apiExpense.category) {
    throw new Error('Category object missing');
  }

  if (!apiExpense.location) {
    throw new Error('Location object missing');
  }

  if (!apiExpense.paymentMethod) {
    throw new Error('Payment method object missing');
  }

  return {
      id: apiExpense.id,
      amount: apiExpense.amount,
      date: dayjs(apiExpense.date),
      category: apiExpense.category.name,
      location: apiExpense.location.name,
      paymentMethod: apiExpense.paymentMethod.name,
      notes: apiExpense.notes ? apiExpense.notes : undefined,
  }
}

/**
 * POST /api/expense
 * @param expense Expense to be created
 */
export async function create(expense: Expense): Promise<APIResponse<number>> {
  const expenseBody = {
    amount: expense.amount,
    date: expense.date.format('YYYY-MM-DD'),
    category: expense.category,
    location: expense.location,
    paymentMethod: expense.paymentMethod,
    notes: expense.notes === '' ? undefined : expense.notes,
  }

  const response = await axios.post<ApiExpense|string>('/expense', expenseBody);

  if (response.status === 200) {
    const responseBody = response.data as ApiExpense;
    if (!responseBody.id) {
      throw new Error("Received a ApiExpense object that doesn't have an ID");
    }

    return {
      data: responseBody.id,
    }
  } else {
    const responseBody = response.data as string;
    const statusCode = response.status;
    return {
      error: `${statusCode}: ${responseBody}`,
    }
  }
}

/**
 * GET /api/expense
 */
export async function getMany(
    filters?: {
      startDate?: dayjs.Dayjs,
      endDate?: dayjs.Dayjs,
    }
  ): Promise<APIResponse<Expense[]>> {

  let url = '/expense'

  if (filters !== undefined) {
    const { startDate, endDate } = filters;
    if (startDate !== undefined) {
      url += `?startDate=${startDate.format('YYYY-MM-DD')}`;
    }
    if (endDate !== undefined) {
      url += `&endDate=${endDate.format('YYYY-MM-DD')}`;
    }
  }


  const response = await axios.get<ApiExpense[]|string>(url);

  if (response.status === 400) {
    // user error
    console.error(response);
    return {
      error: response.data as string,
    }
  }

  const expensesData = response.data as ApiExpense[];

  const expenses = expensesData.map(apiExpenseToExpense);
  return {
    data: expenses,
  }
}

export async function getById(id: number): Promise<APIResponse<Expense>> {
  const response = await axios.get<ApiExpense|string>(`/expense/${id}`);

  if (response.status === 404) {
    return { error: "Not found" };
  }

  if (response.status !== 200) {
    // user or server error
    console.error(response);
    return { error: response.data as string };
  }

  const expense = apiExpenseToExpense(response.data as ApiExpense);
  return { data: expense };
}

/**
 * GET /api/expense/expensesPerCategory
 */
export async function getAggregatedExpenses(): Promise<any> {
  const response = await axios.get<string>('/expense/expensesPerCategory');

  if (response.status >= 400) {
    // user error
    console.error(response);
    return {};
  }

  return JSON.parse(response.data);
}

/**
 * PUT /api/expense/:id
 * @param expense new Expense data with old ID
 */
export async function updateExpense(expense: Expense): Promise<APIResponse<Expense>> {
  if (expense.id === undefined) {
    return { error: 'expense.id cannot be undefined' };
  }

  const expenseBody = {
    id: expense.id,
    amount: expense.amount,
    date: expense.date.format('YYYY-MM-DD'),
    category: expense.category,
    location: expense.location,
    paymentMethod: expense.paymentMethod,
    notes: expense.notes,
  }

  const response = await axios.put<ApiExpense|string>('/expense', expenseBody);

  if (response.status === 404) {
    return { error: 'Not found' };
  }

  if (response.status !== 200) {
    console.error(response);
    return { error: response.data as string };
  }

  const updatedExpense = apiExpenseToExpense(response.data as ApiExpense);
  return { data: updatedExpense };
}

export default {
  create,
  getMany,
  getById,
  getAggregatedExpenses,
  updateExpense,
};
