import axios from '../axios';

export interface ApiPaymentMethod {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export async function getMany(): Promise<string[]> {
  const response = await axios.get<ApiPaymentMethod[]>('/paymentMethod');

  if (response.status >= 400) {
    // user error
    console.error(response);
    return [];
  }

  return response.data.map(pm => pm.name);
}

export default {
  getMany,
}