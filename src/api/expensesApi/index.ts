import expense from './expense';
import category from './category';
import location from './location';
import paymentMethod from './paymentMethod';
import auth from './auth';

export default {
  expense,
  category,
  location,
  paymentMethod,
  auth,
}
