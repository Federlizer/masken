import axios from '../axios';

export interface ApiLocation {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export async function getMany(): Promise<string[]> {
  const response = await axios.get<ApiLocation[]>('/location');

  if (response.status >= 400) {
    // user error
    console.error(response);
    return [];
  }

  return response.data.map(l => l.name);
};

export default {
  getMany,
}
