import axios from '../axios';

export interface ApiUser {
  id: number;
  username: string;
}

export async function authenticate(username: string, password: string): Promise<ApiUser|null> {
  const body = {
    username,
    password,
  }

  const response = await axios.post<ApiUser>('/auth', body);

  if (response.status >= 400) {
    // user error
    console.error(response);
    return null;
  }

  return response.data;
}

export async function getUser(): Promise<ApiUser|null> {
  const response = await axios.get<ApiUser>('/auth');

  if (response.status !== 200) {
    return null;
  }

  return response.data;
}

export default {
  authenticate,
  getUser,
}