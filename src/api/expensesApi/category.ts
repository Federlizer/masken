import axios from '../axios';

export interface ApiCategory {
  id: number;
  name: string;
  createdAt: string;
  updatedAt: string;
}

export async function getMany(): Promise<string[]> {
  const response = await axios.get<ApiCategory[]>('/category');

  if (response.status >= 400) {
    // user error
    console.error(response);
    return [];
  }

  return response.data.map(c => c.name);
};

export default {
  getMany,
};
