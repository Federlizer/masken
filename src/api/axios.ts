import axios from 'axios';
import config from '../config';

const axiosInstance = axios.create({
  validateStatus: (status) => {
    return status < 500;
  },
  baseURL: config.apiUrl,
  withCredentials: true,
});

export default axiosInstance;