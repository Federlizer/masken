import { createRouter, createWebHistory } from 'vue-router';

import authApi from '../api/expensesApi/auth';

import Home from '../pages/Home.vue';
import Login from '../pages/Login.vue';
import Expenses from '../pages/Expenses.vue';
import EditExpense from '../pages/EditExpense.vue';
import NotFound from '../pages/NotFound.vue';

enum RouteNames {
  HOME = 'Home',
  PAYWALL = 'Paywall',
  EXPENSES = 'Expenses',
  EDIT_EXPENSE = 'Edit Expense',
  NOT_FOUND = 'Not Found',
}

const protectedRoutes = [
  RouteNames.EXPENSES,
  RouteNames.EDIT_EXPENSE,
]

const routes = [
  {
    path: '/',
    name: RouteNames.HOME,
    component: Home,
  },
  {
    path: '/paywall',
    name: RouteNames.PAYWALL,
    component: Login,
  },
  {
    path: '/expenses',
    name: RouteNames.EXPENSES,
    component: Expenses,
  },
  {
    path: '/expenses/edit/:id',
    name: RouteNames.EDIT_EXPENSE,
    component: EditExpense,
    props: true,
  },
  {
    path: '/:notFound(.*)*', // magic!
    name: RouteNames.NOT_FOUND,
    component: NotFound,
  },
]

const router = createRouter({
  routes: routes,
  history: createWebHistory(),
});

router.beforeEach(async (to, from) => {
  const protectedRoute = protectedRoutes.find((routeName) => to.name === routeName);
  if (!protectedRoute) {
    return;
  }

  const user = await authApi.getUser();
  if (user === null) {
    return { name: RouteNames.PAYWALL };
  }

  return true;
});

export default router;
