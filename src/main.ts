import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';

import { createApp } from 'vue';

import App from './App.vue';
import router from './router';
import store from './store';

import './index.css';

dayjs.extend(customParseFormat);
dayjs.extend(isSameOrAfter);
dayjs.extend(isSameOrBefore);

const app = createApp(App);

app.use(router);
app.use(store);
app.mount('#app');
