import dayjs from 'dayjs';
import { State } from 'vue';
import { createStore } from 'vuex';

import sortExpenses from '../utils/sortExpenses';
import zeroPadded from '../utils/zeroPadded';

import expenseApi from '../api/expensesApi/expense';

import actions from './actions';
import mutations from './mutations';

import actionPayloadSchemas from '../schemas/storeActions';
import mutationPayloadSchemas from '../schemas/storeMutations';

const store = createStore<State>({
  state() {
    return {
      expenses: {},
      categories: [],
      locations: [],
      paymentMethods: [],
    }
  },

  mutations: {
    [mutations.SET_EXPENSES] (state, payload) {
      const { value, error } = mutationPayloadSchemas.setExpenses.validate(payload);

      if (error) {
        throw error;
      }

      const { monthYear, expenses } = value as VuexMutations.SetExpensesPayload;

      let fetchedFromApi = false;
      let entries: Expense[] = [];

      if (expenses.fetchedFromApi !== undefined) {
        fetchedFromApi = expenses.fetchedFromApi;
      } else if (state.expenses[monthYear] !== undefined) {
        fetchedFromApi = state.expenses[monthYear].fetchedFromApi;
      }

      if (expenses.entries !== undefined) {
        entries = expenses.entries;
      } else if (state.expenses[monthYear] !== undefined) {
        entries = state.expenses[monthYear].entries;
      }

      state.expenses[monthYear] = { fetchedFromApi, entries };
    },

    [mutations.ADD_CATEGORIES] (state, payload) {
      state.categories = [
        ...state.categories,
        ...payload.categories,
      ].sort();
    },

    [mutations.ADD_LOCATIONS] (state, payload) {
      state.locations = [
        ...state.locations,
        ...payload.locations,
      ].sort();
    },

    [mutations.ADD_PAYMENT_METHODS] (state, payload) {
      state.paymentMethods = [
        ...state.paymentMethods,
        ...payload.paymentMethods,
      ].sort();
    },
  },

  getters: {
    getExpenses: (state) => (monthYear: string): Expense[] => {
      if (state.expenses[monthYear] === undefined) {
        return [];
      }

      return state.expenses[monthYear].entries;
    }
  },

  actions: {
    async [actions.FETCH_EXPENSES] (context, payload) {
      const { value, error } = actionPayloadSchemas.fetchExpenses.validate(payload);

      if (error) {
        return error;
      }

      const { state, commit } = context;
      const checkedPayload: VuexActions.FetchExpensesPayload = value;
      const monthYear = `${zeroPadded(checkedPayload.month)}-${checkedPayload.year}`;


      if (state.expenses[monthYear] !== undefined
        && state.expenses[monthYear].fetchedFromApi
        && !checkedPayload.force) {
        // no force, skip operation
        return;
      }

      // leave me until this is very much tested thoroughly
      console.log(`Fetching expenses for ${monthYear}`);

      const startDate = dayjs(`${checkedPayload.year}-${checkedPayload.month}-01`);
      const endDate = startDate.endOf('month');

      const response = await expenseApi.getMany({
        startDate,
        endDate,
      });

      if (response.error) {
        return response.error;
      } else if (response.data === undefined) {
        throw new Error('Didn\'t get error, but also didn\'t get data back from API module');
      }

      const expenses = sortExpenses.byDate(response.data);


      commit(mutations.SET_EXPENSES, {
        monthYear,
        expenses: {
          fetchedFromApi: true,
          entries: expenses,
        },
      });
    },

    /**
     * Creates the expense through the API layer and manipulates the store to reflect the
     * necessary changes. This includes adding the expense and making sure to add any new
     * categories, locations or paymentMethods necessary.
     *
     * @param payload The payload must contain the expense
     * (as a copy, not a reference from the original component data) that is to be created.
     *
     * @returns Returns a string with the error message if the action failed,
     * undefined otherwise.
     */
    async [actions.CREATE_EXPENSE] (context, payload) {
      const { value, error } = actionPayloadSchemas.createExpense.validate(payload);

      if (error) {
        return error.message;
      }

      const { state, commit } = context;
      const checkedPayload: VuexActions.CreateExpensePayload = value;
      const { expense } = checkedPayload;

      // execute API
      const response = await expenseApi.create(expense);

      if (response.error) {
        return response.error;
      } else if (response.data === undefined) {
        throw new Error('Didn\'t get error, but also didn\'t get data back from API module');
      } else {
        const expenseId = response.data;
        expense.id = expenseId;
      }

      const monthYear = expense.date.format('MM-YYYY');

      // Need this check since expenses[monthYear] might not exist at the time
      // of creating a new expense
      let newExpenses: Expense[];
      if (state.expenses[monthYear]) {
        newExpenses = sortExpenses.byDate([
          ...state.expenses[monthYear].entries,
          expense,
        ]);
      } else {
        newExpenses = [ expense ];
      }

      commit(mutations.SET_EXPENSES, {
        monthYear,
        expenses: {
          entries: newExpenses,
        },
      });

      if (!state.categories.find(c => c === expense.category)) {
        commit(mutations.ADD_CATEGORIES, { categories: [expense.category] });
      }

      if (!state.locations.find(l => l === expense.location)) {
        commit(mutations.ADD_LOCATIONS, { locations: [expense.location] });
      }

      if (!state.paymentMethods.find(pm => pm === expense.paymentMethod)) {
        commit(mutations.ADD_PAYMENT_METHODS, { paymentMethods: [expense.paymentMethod] });
      }
    },

    async [actions.UPDATE_EXPENSE] (context, payload) {
      const { value, error } = actionPayloadSchemas.updateExpense.validate(payload);

      if (error) {
        return error;
      }

      const { state, commit } = context;
      const { oldExpense, newExpense } = value as VuexActions.UpdateExpensePayload;

      // execute API
      const response = await expenseApi.updateExpense(newExpense);

      // check API response
      if (response.error) {
        return response.error;
      } else if (response.data === undefined) {
        throw new Error('Didn\'t get error, but also didn\'t get data back from API module');
      } else if (response.data.id === undefined) {
        throw new Error('Received updated expense WITHOUT id field');
      }

      const updatedExpense = response.data;

      const oldMonthYear = oldExpense.date.format('MM-YYYY');
      const updatedMonthYear = updatedExpense.date.format('MM-YYYY');

      if (state.expenses[oldMonthYear] === undefined) {
        throw new Error('Could not find old monthYear entry, bailing update operation');
      }

      if (oldMonthYear === updatedMonthYear) {
        const updatedExpenses = sortExpenses.byDate(state.expenses[oldMonthYear].entries.map(e => {
          return e.id === updatedExpense.id
            ? updatedExpense
            : e;
        }));

        commit(mutations.SET_EXPENSES, {
          monthYear: oldMonthYear,
          expenses: {
            entries: updatedExpenses,
          },
        });
      } else {
        // find old expense, delete it
        commit(mutations.SET_EXPENSES, {
          monthYear: oldMonthYear,
          expenses: {
            entries: state.expenses[oldMonthYear].entries.filter(e => e.id !== oldExpense.id),
          },
        });

        // add new expense to proper expense[updatedMonthYear]
        const updatedExpenses = state.expenses[updatedMonthYear] === undefined
          ? [updatedExpense]
          : sortExpenses.byDate([...state.expenses[updatedMonthYear].entries, updatedExpense]);

        commit(mutations.SET_EXPENSES, {
          monthYear: updatedMonthYear,
          expenses: {
            entries: updatedExpenses,
          },
        });
      }

      // add new category, location and paymentMethod
      if (!state.categories.find(c => c === updatedExpense.category)) {
        commit(mutations.ADD_CATEGORIES, { categories: [updatedExpense.category] });
      }

      if (!state.locations.find(l => l === updatedExpense.location)) {
        commit(mutations.ADD_LOCATIONS, { locations: [updatedExpense.location] });
      }

      if (!state.paymentMethods.find(pm => pm === updatedExpense.paymentMethod)) {
        commit(mutations.ADD_PAYMENT_METHODS, { paymentMethods: [updatedExpense.paymentMethod] });
      }
    },
  }
});

export default store;
