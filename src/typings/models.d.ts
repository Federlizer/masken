type APIResponse<T> = {
  data: T;
  error?: never;
} | {
  data?: never;
  error: string;
}

interface Expense {
  id?: number;
  date: import('dayjs').Dayjs;
  amount: number;
  category: string;
  location: string;
  paymentMethod: string;
  notes?: string;
}