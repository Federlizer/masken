import { ComponentCustomProperties } from 'vue';
import { Store } from 'vuex';

declare module '@vue/runtime-core' {
  type State = VuexStoreState;

  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
