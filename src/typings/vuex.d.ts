/**
 * The expenses object contains one object per month-year combo.
 * The format of the monthYear directive is MM-YYYY (dayjs formatting).
 * MM is 01 indexed, meaning 01 is January, 12 is December, contrary to
 * popular 0 based indexes (dayjs objects and plain JS Date objects).
 */
interface VuexStoreState {
  categories: string[];
  locations: string[];
  paymentMethods: string[];

  expenses: {
    [monthYear: string]: {
      fetchedFromApi: boolean;
      entries: Expense[];
    }
  }
}

namespace VuexMutations {
  interface SetExpensesPayload {
    monthYear: string;
    expenses: {
      fetchedFromApi?: boolean;
      entries?: Expense[];
    };
  }
}

namespace VuexActions {
  import { Dayjs } from 'dayjs';

  interface FetchExpensesPayload {
    month: number;
    year: number;
    force?: boolean;
  }

  interface CreateExpensePayload {
    expense: Expense;
  }

  interface UpdateExpensePayload {
    oldExpense: Expense;
    newExpense: Expense;
  }
}
