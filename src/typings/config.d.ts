interface EnvironmentConfiguration {
  apiUrl: string;
}

interface Configuration {
  [environment: string]: EnvironmentConfiguration;
}
