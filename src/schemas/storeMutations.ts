import Joi from 'joi';

import { FullExpenseSchema } from './expense';

export const setExpensesPayloadSchema = Joi.object({
  monthYear: Joi
    .string()
    .regex(/(0[1-9]|1[0-2])-[0-9]{4}/)
    .required(),

  expenses: Joi
    .object({
      fetchedFromApi: Joi.boolean(),
      entries: Joi.array().items(FullExpenseSchema),
    })
    .required(),
});


export default {
  setExpenses: setExpensesPayloadSchema,
}
