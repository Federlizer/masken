import Joi from 'joi';

import { ExpenseSchema, FullExpenseSchema } from './expense';

export const fetchExpensesPayloadSchema = Joi.object({
  month: Joi
    .number()
    .min(1)
    .max(12)
    .required(),

  year: Joi
    .number()
    .positive()
    .required(),

  force: Joi
    .boolean()
    .optional(),
});

export const createExpensePayloadSchema = Joi.object({
  expense: ExpenseSchema
    .required(),
});

export const updateExpensePayloadSchema = Joi.object({
  oldExpense: FullExpenseSchema
    .required(),

  newExpense: FullExpenseSchema
    .required(),
});

export default {
	fetchExpenses: fetchExpensesPayloadSchema,
  createExpense: createExpensePayloadSchema,
  updateExpense: updateExpensePayloadSchema,
}
