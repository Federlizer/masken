import Joi from 'joi';

export const ExpenseSchema = Joi.object({
  id: Joi
    .number()
    .positive()
    .optional(),

  // NOTE: is a Dayjs object, don't know how to validate properly
  date: Joi
    .object()
    .required(),

  amount: Joi
    .number()
    .positive()
    .required(),

  category: Joi
    .string()
    .required(),

  location: Joi
    .string()
    .required(),

  paymentMethod: Joi
    .string()
    .required(),

  notes: Joi
    .string()
    .max(500)
    .optional(),
});

export const FullExpenseSchema = ExpenseSchema.keys({
  id: Joi
    .number()
    .positive()
    .required()
});

export default {
  ExpenseSchema,
  FullExpenseSchema,
};
