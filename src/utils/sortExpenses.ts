export const byDate = (expenses: Expense[]): Expense[] => {
  return expenses.sort((a, b) => {
    if (a.date.isBefore(b.date)) {
      return 1;
    }
    if (a.date.isAfter(b.date)) {
      return -1;
    }
    return 0;
  });
};

export default {
  byDate,
}