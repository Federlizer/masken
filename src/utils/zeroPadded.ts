
export const zeroPadded = (month: number): string => {
	return month < 10 ? `0${month}` : `${month}`;
}

export default zeroPadded;
