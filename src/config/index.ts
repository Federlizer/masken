import production from './production';
import development from './development';

const env = process.env.NODE_ENV as string;

const configs: Configuration = {
  production,
  development,
};

export default configs[env];
