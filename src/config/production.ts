const production: EnvironmentConfiguration = {
  apiUrl: 'https://federlizer.com/api',
};

export default production;
