const development: EnvironmentConfiguration = {
  apiUrl: 'http://localhost:8000/api',
};

export default development;
